package com.charles.enums;

/**
 * @author charles
 * @date 2023/4/29 13:52
 */
public enum ChannelTypeEnum {
    SMS, PUSH;
}
