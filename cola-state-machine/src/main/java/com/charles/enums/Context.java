package com.charles.enums;

import lombok.Data;

/**
 * 参数
 */
@Data
public class Context {
    String operator = "flw";
    String entityId = "7758258";
    String name = "zhoumin";
}