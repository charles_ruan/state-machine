package com.charles.enums;

public enum States {
    /**
     * 申请
     */
    APPLY(1, "申请"),
    /**
     * 初审
     */
    FIRST_TRIAL(2, "初审"),
    /**
     * 终审
     */
    FINAL_JUDGMENT(3, "终审"),
    /**
     * 移出
     */
    REMOVE(4, "移出");

    States(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    private final Integer value;

    private final String name;

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

}