package com.charles.enums;

public enum Events {
        /**
         * 保存申请
         */
        SAVE_APPLICATION,
        /**
         * 提交申请
         */
        SUBMIT_APPLICATION,
        /**
         * 审核通过
         */
        AUDIT_PASS,
        /**
         * 审核退回
         */
        AUDIT_REJECT
    }