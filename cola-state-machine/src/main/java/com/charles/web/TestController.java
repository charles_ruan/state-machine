package com.charles.web;

import com.charles.engine.StatusMachineEngine;
import com.charles.enums.ChannelTypeEnum;
import com.charles.enums.Context;
import com.charles.enums.Events;
import com.charles.enums.States;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author charles
 * @date 2023/4/29 14:10
 */
@RestController
public class TestController {

    @RequestMapping("/t1")
    public void t1() {
        StatusMachineEngine.fire(ChannelTypeEnum.SMS, States.APPLY, Events.SAVE_APPLICATION, new Context());
    }
}
