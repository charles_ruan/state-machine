package com.charles.service;

import com.alibaba.cola.statemachine.Action;
import com.charles.enums.Context;
import com.charles.enums.Events;
import com.charles.enums.States;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author charles
 * @date 2023/4/29 14:04
 */
@Service
@Slf4j
public class StatusAction {
    public Action<States, Events, Context> doNotifyAction() {
        return (from, to, event, ctx) -> log.info(ctx.getOperator() + " is operating " + ctx.getEntityId() + " from:" + from.getName() + " to:" + to.getName() + " on:" + event + ";" + ctx.getName());
    }
}
