package com.charles.service;

import com.alibaba.cola.statemachine.Condition;
import com.charles.enums.Context;
import org.springframework.stereotype.Service;

/**
 * @author charles
 * @date 2023/4/29 14:05
 */
@Service
public class StatusCondition {

    public Condition<Context> checkNotifyCondition() {
        return (ctx) -> true;
    }
}
