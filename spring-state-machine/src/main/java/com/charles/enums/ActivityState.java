package com.charles.enums;

public enum ActivityState {
    NOT_START(0),
    DATA_PREPARING(1),
    DATA_PREPARED(2),
    DATA_PUSHING(3),
    FINISHED(4);

    private int state;

    ActivityState(int state) {
        this.state = state;
    }
}