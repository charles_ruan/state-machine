package com.charles.enums;

public enum ActEvent {
    ACT_BEGIN, FINISH_DATA_CAL, FINISH_DATA_PREPARE, FINISH_DATA_PUSHING
}