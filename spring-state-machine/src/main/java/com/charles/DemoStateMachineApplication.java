package com.charles;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.charles.mapper")
public class DemoStateMachineApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoStateMachineApplication.class, args);
    }

}
